# Classwork

> Safely sets, unsets, & replaces classes on an HTMLElement.

## Install

```bash
npm i @darkkenergy/classwork -S
```

## Inclusion

```js
// CommonJS
var Classwork = require('@darkkenergy/classwork');

// ES6
import Classwork from '@darkkenergy/classwork';
```

## Usage

```js
/**
 * @name Classwork
 * @desc Safely sets, unsets, & replaces classes on an HTMLElement.
 *
 * @param el {HTMLElement} :The element to which classes will be managed.
 * @param oldClass {string} :A class to add, remove, or replace.
 * @param newClass {string} :A replacement class - is simply added if the old class doesn't exist.
 *
 * @return {string} :The new class name of the element - empty if no element was provided.
 */

// Classwork(el, oldClass, newClass)

import Classwork from '@darkkenergy/classwork';

// Toggle (Add/Remove)
Classwork(element, class);

// Add
Classwork(element, null, class);

// Replace
Classwork(element, oldClass, newClass);
```

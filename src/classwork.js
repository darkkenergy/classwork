var HTMLElement = require('element-wrappers').HTMLElement();

/**
 * @name Classwork
 * @version 1.0.5
 * @author Claudio Nuñez Jr.
 * @desc Safely sets, unsets, & replaces classes on an HTMLElement.
 *
 * @param el {HTMLElement} :The element to which classes will be managed.
 * @param oldClass {string} :A class to add, remove, or replace.
 * @param newClass {string} :A replacement class - is simply added if the old
 *      class doesn't exist.
 *
 * @return {string} :The new class name of the element - empty if no element was
 *      provided.
 */
function Classwork(el, oldClass, newClass) {
    var className,
        classGroup,
        classIndex;

    if (!(el instanceof HTMLElement)) {

        // An HTMLElement was not provided.

        // Return a safe value.
        className = "";
        return className;
    };

    // Get the class name.
    className = el.className;
    classGroup = className.split(" ");
    classIndex = classGroup.indexOf(oldClass);

    // Work on the class name.
    if (classIndex > -1) {

        // Replaces or removes the old class.
        classGroup[classIndex] = (newClass || "").trim();
        className = classGroup.join(" ");
    } else {

        // Adds the new class, old class, or nothing, in that order.
        className = (className + " " + (newClass || oldClass || "")).trim();
    }

    // Set the new class name on the element.
    el.className = className;

    return className;
}

module.exports = Classwork;
